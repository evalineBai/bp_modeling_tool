const request = require('request');
const fs = require('fs');
const config = require('../config.js');

const saveToCSV = (obj) => {
  fs.appendFile('solarData.csv', `${obj}\n\n`, 'utf8', (err) => {
    if (err) {
      console.log('Some error occured - file either not saved or corrupted file saved.');
    } else {
      console.log('Data was saved to csv!');
    }
  });
}

const generateSolarData = (lat, long, arrayType, callback) => {
  let options = {
    url: `https://developer.nrel.gov/api/pvwatts/v6.json?api_key=${config.KEY}&lat=${lat}&lon=-${long}&system_capacity=25&azimuth=180&tilt=25&array_type=${arrayType}&module_type=1&losses=13`,
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'User-Agent': 'request'
    }
  }
  request(options, (error, response, body) => {
    if (error) throw error;
    let data = JSON.parse(body);
    callback(data);
  });
}

module.exports.generateSolarData = generateSolarData;
module.exports.saveToCSV = saveToCSV;