const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const cors = require('cors');
const helper = require('./helper.js');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/../client/dist'));
app.use(cors());

app.post('/data', (req, res) => {
  console.log(req.body);
  let lat = req.body.lat;
  let long = req.body.long;
  let arrayType = req.body.arrayType;
  helper.generateSolarData(lat, long, arrayType, (data) => {
    helper.saveToCSV(JSON.stringify(data));
    res.send(JSON.stringify(data));
  });
});

const port = 3000;

app.listen(port, function () {
  console.log(`listening on port ${port}`);
});

