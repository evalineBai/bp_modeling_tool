import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';
import { Grid } from 'semantic-ui-react';
import Nav from './Nav';
import UserForm from './Form';
import DataTable from './Table';
import SimpleMap from './SimpleMap';
import Graph from './Graph';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      lat: 41,
      long: 87,
      solRadMonthly: [],
      solRadAnnual: 0,
      acMonthly: [],
      acAnnual: 0,
      graphData: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        datasets: [
          {
            label: 'Monthly Solar Radiation (kWh/m2/day)',
            data: [],
            backgroundColor: 'rgba(0, 0, 225, 0.1)'
          }
        ]
      }
    }
    this.generateData = this.generateData.bind(this);
  }

  generateData(lat, long, arrayType) {
    this.setState({
      lat: lat,
      long: long
    });
    axios.post('/data', {
      lat: `${lat}`,
      long: `${long}`,
      arrayType: `${arrayType}`
    })
    .then(res => {
      const newData = this.state.graphData;
      newData.datasets[0].data = res.data.outputs.solrad_monthly;
      newData.datasets[0].data = res.data.outputs.solrad_monthly;
      this.setState({
        solRadMonthly: res.data.outputs.solrad_monthly,
        solRadAnnual: res.data.outputs.solrad_annual,
        acMonthly: res.data.outputs.ac_monthly,
        acAnnual: res.data.outputs.ac_annual,
        graphData: newData
      });
    })
    .catch(err => {
      console.log(`Error grabbing data: ${err}`);
    });
  }

  render() {
    return (
      <div style={{ fontFamily: 'sans-serif' }}>
        <Nav />
        <div style={{ margin: '3% 5% 5% 5%' }}>
          <h3>Enter the location and type of proposed installation:</h3>
          <UserForm generateData={this.generateData} />
          <DataTable
            solRadAnnual={this.state.solRadAnnual}
            acAnnual={this.state.acAnnual}
            solRadMonthly={this.state.solRadMonthly}
            acMonthly={this.state.acMonthly}
          />
          <Grid columns={2}>
            <Grid.Row>
              <Grid.Column>
                <SimpleMap
                  lat={this.state.lat}
                  long={this.state.long}                 
                />
              </Grid.Column>
              <Grid.Column>
                <Graph graphData={this.state.graphData} />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </div>
    );
  };
}

ReactDOM.render(<App />, document.getElementById('app'));
