import React from 'react'
import { Icon, Label, Menu, Table } from 'semantic-ui-react'

const DataTable = ({ solRadAnnual, acAnnual, solRadMonthly, acMonthly }) => {
  const zeros = [0,0,0,0,0,0,0,0,0,0,0,0];
  const EmptyTable = zeros.map((el) => {
    return (<Table.Cell>{el}</Table.Cell>);
  });
  const SolRadTable = solRadMonthly.map((num) => {
    return (<Table.Cell>{Number.parseFloat(num).toFixed(2)}</Table.Cell>)
  });
  const AcTable = acMonthly.map((num) => {
    return (<Table.Cell>{Number.parseFloat(num).toFixed(2)}</Table.Cell>)
  });
  const valueTable = acMonthly.map((num) => {
    return (<Table.Cell>{Number.parseFloat(num * 0.12).toFixed(2)}</Table.Cell>)
  });
  const FinalSolTable = solRadMonthly.length === 0 ? EmptyTable : SolRadTable;
  const FinalAcTable = acMonthly.length === 0 ? EmptyTable : AcTable;
  const FinalValueTable = acMonthly.length === 0 ? EmptyTable : valueTable;
  return (
  <Table celled>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>
          <Label ribbon>Monthly Metric</Label>
        </Table.HeaderCell>
        <Table.HeaderCell>Jan</Table.HeaderCell>
        <Table.HeaderCell>Feb</Table.HeaderCell>
        <Table.HeaderCell>Mar</Table.HeaderCell>
        <Table.HeaderCell>Apr</Table.HeaderCell>
        <Table.HeaderCell>May</Table.HeaderCell>
        <Table.HeaderCell>Jun</Table.HeaderCell>
        <Table.HeaderCell>Jul</Table.HeaderCell>
        <Table.HeaderCell>Aug</Table.HeaderCell>
        <Table.HeaderCell>Sep</Table.HeaderCell>
        <Table.HeaderCell>Oct</Table.HeaderCell>
        <Table.HeaderCell>Nov</Table.HeaderCell>
        <Table.HeaderCell>Dec</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      <Table.Row>
        <Table.Cell>Solar Radiation (kWh/m2/day)</Table.Cell>
        {FinalSolTable}
      </Table.Row>

      <Table.Row>
        <Table.Cell>AC Energy (kWhac)</Table.Cell>
        {FinalAcTable}
      </Table.Row>

      <Table.Row>
        <Table.Cell>Value ($)</Table.Cell>
        {FinalValueTable}
      </Table.Row>
    </Table.Body>
    <Table.Footer>
      <Table.Row>
        <Table.HeaderCell colSpan='13'>
            <span
              style={{ marginRight: '4%'}}
            >
              <strong>Annual Solar Radiation (kWh/m2): </strong> {Number.parseFloat(solRadAnnual).toFixed(2)}
            </span>
            <span
              style={{ marginRight: '4%' }}
            >
              <strong>Annual AC Energy (kWh): </strong>{Number.parseFloat(acAnnual).toFixed(2)}
            </span>
            <span><strong>Annual Value ($): </strong>{Number.parseFloat(acAnnual * 0.12).toFixed(2)}</span>
        </Table.HeaderCell>
      </Table.Row>
    </Table.Footer>
  </Table>
  );
};

export default DataTable;