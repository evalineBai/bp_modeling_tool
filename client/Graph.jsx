import React from 'react';
import { Line } from 'react-chartjs-2';

const Graph = ({graphData}) => (
  <div
    style={{
      height: '70vh',
      width: '100%',
      marginTop: '1%'
    }}
  >
    <Line
      data={graphData}
      options={{
        maintainAspectRatio: false
      }}
    />
  </div>
);

export default Graph;