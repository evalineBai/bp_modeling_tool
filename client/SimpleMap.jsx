import React from 'react';
import { Icon } from 'semantic-ui-react';
import GoogleMapReact from 'google-map-react';
import Key from '../config.js';

const SimpleMap = ({ lat, long }) => (
  <div
    style={{
      height: '65vh',
      width: '100%',
      marginTop: '5%'
    }}
  >
    <GoogleMapReact
      bootstrapURLKeys={{ key: Key.GKEY }}
      defaultCenter={{ lat: lat, lng: long }}
      defaultZoom={11}
    >
    <Icon
      circular
      size='huge'
      name='power cord'
      lat={lat}
      lng={long}
      />
    </GoogleMapReact>
  </div>
);

export default SimpleMap;