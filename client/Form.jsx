import React from 'react';
import { Form, Button } from 'semantic-ui-react'

const options = [
  { key: '0', text: 'Fixed - Open Rack', value: '0' },
  { key: '1', text: 'Fixed - Roof Mounted', value: '1' },
  { key: '2', text: '1-Axis', value: '2' },
  { key: '3', text: '1-Axis Backtracking', value: '3' },
  { key: '4', text: '2-Axis', value: '4' }
]
class UserForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      lat: '',
      long: '',
      arrayType: ''
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleDropdown(e, result) {
    const { name, value } = result;
    this.setState({
      arrayType: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    console.log('lat:', this.state.lat);
    console.log('long:', this.state.long);
    console.log('arrayType:', this.state.arrayType);
    this.props.generateData(this.state.lat, this.state.long, this.state.arrayType);
  }

  render() {
    const { value } = this.state
    return (
      <Form>
        <Form.Group widths='equal'>
          <Form.Input
            fluid
            type='text'
            name='lat'
            placeholder='Latitude (e.g. 40)'
            onChange={this.handleChange}
          />
          <Form.Input
            fluid
            type='text'
            name='long'
            placeholder='Longitude (e.g. 74)'
            onChange={this.handleChange}
          />
          <Form.Select
            fluid
            name='arrayType'
            placeholder='Array Type'
            options={options}
            onChange={this.handleDropdown}
          />
          <Button
            fluid
            onClick={this.handleSubmit}
          >
            Calculate
          </Button>
        </Form.Group>
      </Form>
    )
  }
}

export default UserForm;