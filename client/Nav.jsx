import React from 'react';
import { Menu } from 'semantic-ui-react';
import Logo from './media/LOGO.png';

const Nav = () => (
  <Menu
    borderless
    style={{ borderRadius: 0 }}
  >
    <img
      src={Logo}
      alt="Blueprint Power"
      style={{
        width: '75px',
        height: '75px'
      }}
    />
    <Menu.Item
      position="right"
      name="SOLAR ENERGY CALCULATOR"
      style={{
        fontSize: 18,
        color: '#C0C0C0'
      }}
    />
  </Menu>
);

export default Nav;