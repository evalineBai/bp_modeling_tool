# bp_modeling_tool

> Web dashboard that models projected solar energy generation from particular installation and projects the value of generated electricity per year.

## Local Setup

* Clone repo to local environment
* Create a config.js file in root folder
* Create an API key from https://developer.nrel.gov/
* Create a (Google) Maps JavaScript API key
* Populate config.js with your API key:

```
module.exports = {
  KEY: 'YOUR NREL KEY HERE',
  GKEY: 'YOUR GOOGLE KEY HERE'
};
```
* In command line, run npm scripts:

``` 
npm install   
npm run server-dev
npm run react-dev
```
* Navigate to localhost:3000

